# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, _

class Warehouse(models.Model):
    _inherit = "stock.warehouse"

    return_type_id = fields.Many2one('stock.picking.type', 'Return Type', check_company=True)

    def _get_sequence_values(self):
        sequence_values = super(Warehouse, self)._get_sequence_values()
        sequence_values.update({
            'return_type_id': {
                'name': self.name + ' ' + _('Sequence return'),
                'prefix': self.code + '/RET/',
                'padding': 5,
                'company_id': self.company_id.id,
            }
        })
        return sequence_values

    def _get_picking_type_update_values(self):
        picking_type_update_values = super(Warehouse, self)._get_picking_type_update_values()
        picking_type_update_values.update({
            'return_type_id': {'default_location_src_id': self.env.ref('stock.stock_location_customers').id}
        })
        return picking_type_update_values

    def _get_picking_type_create_values(self, max_sequence):
        picking_type_create_values, max_sequence = super(Warehouse, self)._get_picking_type_create_values(max_sequence)
        picking_type_create_values.update({
            'return_type_id': {
                'name': _('Returns'),
                'code': 'incoming',
                'default_location_src_id': self.env.ref('stock.stock_location_customers').id,
                'default_location_dest_id': self.lot_stock_id.id,
                'sequence': max_sequence + 1,
                'sequence_code': 'RET',
                'company_id': self.company_id.id,
            }
        })
        return picking_type_create_values, max_sequence + 2